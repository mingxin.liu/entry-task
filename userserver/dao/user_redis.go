package dao

import (
	"context"
	"encoding/json"
	"entryTask/userserver/config"
	"entryTask/userserver/log"
	"entryTask/userserver/model"
	"github.com/go-redis/redis/v8"
	"strconv"
	"time"
)

type UserRedis struct {
}

var client *redis.Client

func init() {
	client = redis.NewClient(&redis.Options{
		Addr:     config.RedisAddr,
		Password: config.RedisPwd,
		DB:       0,
		PoolSize: config.RedisPoolSize,
	})

	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*1))
	defer cancel()
	_, err := client.Ping(ctx).Result()
	if err != nil {
		panic(err)
	}
	log.Loger.Println("redis init success")
}

func (u UserRedis) Set(user model.User, durationTime time.Duration) error {
	jsonUser, err := json.Marshal(user)
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	err = client.Set(ctx, "user_"+strconv.FormatInt(user.UserId, 10), string(jsonUser), durationTime).Err()
	return err

}

func (u UserRedis) Get(userId int64) (user model.User, err error) {
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	values, err := client.Get(ctx, "user_"+strconv.FormatInt(userId, 10)).Result()
	if err != nil {
		return user, err
	}
	err = json.Unmarshal([]byte(values), &user)
	return user, err
}

func (u UserRedis) Delete(userId int64) error {
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	err := client.Del(ctx, "user_"+strconv.FormatInt(userId, 10)).Err()
	return err
}

func (u UserRedis) SetToken(userId int64, token string, durationTime time.Duration) error {
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	err := client.Set(ctx, "token_"+strconv.FormatInt(userId, 10), token, durationTime).Err()
	if err != nil {
		return err
	}
	return nil
}

func (u UserRedis) GetToken(userId int64) (token string, err error) {
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	token, err = client.Get(ctx, "token_"+strconv.FormatInt(userId, 10)).Result()
	if err == redis.Nil || err != nil {
		return "", err
	}
	return token, nil
}

func (u UserRedis) DelToken(userId int64) (err error) {
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	err = client.Del(ctx, "token_"+strconv.FormatInt(userId, 10)).Err()
	return err
}
