package dao

import (
	"context"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"entryTask/userserver/config"
	"entryTask/userserver/log"
	"entryTask/userserver/model"
	"errors"
	_ "github.com/go-sql-driver/mysql"
	"time"
)

type UserDB struct {
}

var db *sql.DB

func init() {
	var err error
	db, err = sql.Open("mysql", config.MysqlDB)
	if err != nil {
		log.Loger.Fatalln("Open database error: %s\n", err)
	}

	db.SetConnMaxLifetime(config.ConnMaxLifetime)
	db.SetMaxIdleConns(config.MaxIdleConns)
	db.SetMaxOpenConns(config.MaxOpenConns)
	err = db.Ping()
	if err != nil {
		log.Loger.Fatalln(err)
	}
	log.Loger.Println(db)
	log.Loger.Println("mysql init done.")
}

// Add 创建账号 tbl_user
func (u UserDB) Add(user model.User) error {
	//加密password
	log.Loger.Println("mysql: add start")
	sha := sha256.New()
	sha.Write([]byte(user.Password))
	pwd := hex.EncodeToString(sha.Sum(nil))
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	_, err := db.ExecContext(ctx, "INSERT INTO tbl_user (user_name, password, nick_name) values(?, ? ,?)", user.Username, pwd, user.Nickname)
	if err != nil {
		log.Loger.Println("mysql: Add Exec failed")
		log.Loger.Println(err)
		return errors.New("create an account failed")
	}

	log.Loger.Println("mysql: add success")
	return nil
}

func (u UserDB) GetByUsername(username string) (user model.User, err error) {
	user.Username = username
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	rows := db.QueryRowContext(ctx, "SELECT id, nick_name, pic_name, password FROM tbl_user WHERE user_name = ?", username)
	err = rows.Scan(&user.UserId, &user.Nickname, &user.Picture, &user.Password)
	if err != nil {
		log.Loger.Println("mysql: Get scan err: ", err)
		return user, err
	}
	log.Loger.Println("mysql: GetProfile success")
	return user, nil
}

// Get 获取用户信息
func (u UserDB) Get(userid int64) (user model.User, err error) {
	user.UserId = userid
	// 设置超时时间
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
	defer cancel()
	rows := db.QueryRowContext(ctx, "SELECT user_name, nick_name, pic_name, password FROM tbl_user WHERE id = ?", userid)
	err = rows.Scan(&user.Username, &user.Nickname, &user.Picture, &user.Password)
	if err != nil {
		log.Loger.Println("mysql: Get scan err: ", err)
		return user, err
	}
	log.Loger.Println("mysql: GetProfile success")
	return user, nil
}

// Update 更新用户信息
func (u UserDB) Update(user model.User) error {
	log.Loger.Println("mysql: Update start")
	if user.Nickname != "" {
		// 设置超时时间
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
		defer cancel()
		_, err := db.ExecContext(ctx, "UPDATE tbl_user SET nick_name = ? where id = ?", user.Nickname, user.UserId)
		if err != nil {
			log.Loger.Println("mysql: UpdateNickname Exec failed")
			return errors.New("UpdateNickname failed")
		}
		return nil
	}
	if user.Picture != "" {
		// 设置超时时间
		ctx, cancel := context.WithTimeout(context.Background(), time.Duration(time.Second*3))
		defer cancel()
		_, err := db.ExecContext(ctx, "UPDATE tbl_user SET pic_name = ? where id = ?", user.Picture, user.UserId)
		if err != nil {
			log.Loger.Println("mysql: UpdatePicture Exec failed")
			return errors.New("UpdatePicture failed")
		}
		return nil
	}
	return nil
}
