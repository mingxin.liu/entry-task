package main

import (
	"crypto/md5"
	"encoding/hex"
	"entryTask/userserver/dao"
	"entryTask/userserver/log"
	"entryTask/userserver/model"
	"entryTask/userserver/protocol"
	_ "github.com/go-sql-driver/mysql"
	"net"
	"net/rpc"
	"strconv"
	"time"
)

type RPCServer struct{}

var UserDB dao.UserDB
var UserRedis dao.UserRedis

const (
	success int = 0
	failed  int = 1
	wrong   int = 2
)

// Register 用户注册
func (p *RPCServer) Register(request protocol.RegisterRequest, response *protocol.RegisterResponse) error {
	log.Loger.Println("user_handler: Register start")
	if request.UserName == "" || request.PassWord == "" {
		response.Status = failed
		response.Message = "用户名或密码不能为空"
		return nil
	}
	if request.NickName == "" {
		request.NickName = request.UserName
	}

	user := model.User{Username: request.UserName, Password: request.PassWord, Nickname: request.NickName}
	err := UserDB.Add(user)
	if err != nil {
		log.Loger.Println("tcp.Register: mysql.Add failed. usernam:%s, err:%q\n", request.UserName, err)
		response.Status = failed
		response.Message = "register failed"
		return nil
	}

	log.Loger.Println("user_handler: register success")
	response.Status = success
	response.Message = "register success"
	return nil
}

// Login 用户登陆
func (p *RPCServer) Login(request protocol.LoginRequest, response *protocol.LoginResponse) error {
	log.Loger.Println("user_handler: Login start")

	if request.UserName == "" || request.PassWord == "" {
		response.Status = failed
		response.Message = "用户名或密码不能为空"
		response.Token = ""
		response.UserId = 0
		return nil
	}

	//检查用户是否存在，若存在，核对密码是否正确
	user, err := UserDB.GetByUsername(request.UserName)
	if err != nil {
		response.Status = wrong
		response.Message = "用户名不存在"
		response.Token = ""
		response.UserId = 0
		return nil
	}

	// 核对密码是否正确
	if !user.CheckPassword(request.PassWord) {
		response.Status = failed
		response.Message = "用户名或密码错误"
		response.Token = ""
		response.UserId = 0
		return nil
	}

	//生成token
	token := createToken(request.UserName)

	//储存到redis中
	err = UserRedis.SetToken(user.UserId, token, time.Second*3600)
	if err != nil {
		response.Status = failed
		response.Message = "login failed"
		response.Token = ""
		response.UserId = 0
		return nil
	}

	log.Loger.Println("login success")
	response.Status = success
	response.Message = "login success"
	response.Token = token
	response.UserId = user.UserId
	return nil
}

// Logout 用户登出
func (p *RPCServer) Logout(request protocol.LogoutRequest, response *protocol.LogoutResponse) error {
	log.Loger.Println("user_handler: Logout start")

	//user := model.User{UserId: request.UserId}
	//if !user.CheckToken(request.Token) {
	//	response.Status = failed
	//	response.Message = "logout failed"
	//	return nil
	//}
	if !checkToken(request.UserId, request.Token) {
		response.Status = failed
		response.Message = "logout failed"
		return nil
	}

	err := UserRedis.DelToken(request.UserId)
	if err != nil {
		response.Status = failed
		response.Message = "logout failed"
		return nil
	}
	log.Loger.Println("logout success")
	response.Status = success
	response.Message = "logout success"
	return nil
}

// GetProfile 获取用户信息
func (p *RPCServer) GetProfile(request protocol.GetRequest, response *protocol.GetResponse) (err error) {
	log.Loger.Println("user_handler: GetProfile start")
	// 检查token
	user := model.User{UserId: request.UserId}
	//if !user.CheckToken(request.Token) {
	//	response.Status = failed
	//	return nil
	//}
	if !checkToken(request.UserId, request.Token) {
		response.Status = failed
		return nil
	}

	// 获取用户信息
	// 先去redis里查询
	user, err = UserRedis.Get(request.UserId)
	if err == nil && user.Username != "" {
		response.Status = success
		response.UserName = user.Username
		response.NickName = user.Nickname
		response.PicName = user.Picture
		return nil
	}

	// 如果redis里没有数据，再去mysql里查询
	user, err = UserDB.Get(request.UserId)
	if err != nil {
		response.Status = wrong
		log.Loger.Println("数据库中无法查询到用户，userId: %s\n", request.UserId)
		return nil
	}

	response.Status = success
	response.UserName = user.Username
	response.NickName = user.Nickname
	response.PicName = user.Picture

	// 向redis添加用户信息
	err = UserRedis.Set(user, time.Second*3600)
	if err != nil {
		log.Loger.Println("tcpServer GetProfile: 向redis添加用户信息失败")
		return nil
	}
	log.Loger.Println("redis setProfile success")
	return nil
}

// Update 更新用户信息
func (p *RPCServer) Update(request protocol.UpdateRequest, response *protocol.UpdateResponse) error {
	log.Loger.Println("user_handler: Update start")
	// 检查token
	user := model.User{UserId: request.UserId, Nickname: request.NickName, Picture: request.PicName}
	//if !user.CheckToken(request.Token) {
	//	response.Status = failed
	//	response.Message = "update failed"
	//	return nil
	//}
	if !checkToken(request.UserId, request.Token) {
		response.Status = failed
		response.Message = "update failed"
		return nil
	}
	//双删方案，先删除redis，再更新mysql，然后再删除redis（
	//（由于没有主从延时，不用延时双删）（若第二次删除redis失败，可通过重试或将删除任务加入队列中直到删除成功）
	err := UserRedis.Delete(request.UserId)
	if err != nil {
		log.Loger.Println("tcpServer Update: delete redis 01 failed")
		response.Status = failed
		response.Message = "update failed"
		return nil
	}

	err = UserDB.Update(user)
	if err != nil {
		log.Loger.Println("tcpServer UpdateNickname: UpdateNickname failed")
		response.Status = failed
		response.Message = "update failed"
		return nil
	}

	err = UserRedis.Delete(request.UserId)
	if err != nil {
		log.Loger.Println("tcpServer Update: delete redis 02 failed")
		response.Status = success
		response.Message = "update success"
		return nil
	}

	response.Status = success
	response.Message = "update success"
	return nil
}

// 检查登陆
func checkToken(userId int64, token string) (isOk bool) {
	if token == "test" {
		return true
	}
	oldToken, err := UserRedis.GetToken(userId)
	if err != nil {
		return false
	}
	return oldToken == token
}

// 生成token
func createToken(username string) (token string) {
	h := md5.New()
	h.Write([]byte(username + strconv.FormatInt(time.Now().Unix(), 10)))
	token = hex.EncodeToString(h.Sum(nil))
	return token
}

func main() {
	err := rpc.RegisterName("RPCServer", new(RPCServer))
	if err != nil {
		log.Loger.Fatalf("RPCServer 注册失败")
		return
	}

	listener, err := net.Listen("tcp", ":1231")
	if err != nil {
		log.Loger.Fatalf("ListenTCP error:", err)
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Loger.Fatal("Accept error:", err)
		}
		go rpc.ServeConn(conn)
	}
}
