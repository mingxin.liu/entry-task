package model

import (
	"crypto/sha256"
	"encoding/hex"
)

type User struct {
	UserId   int64
	Username string
	Password string
	Nickname string
	Picture  string
}

func (u User) CheckPassword(pwd string) bool {
	sha := sha256.New()
	sha.Write([]byte(pwd))
	pwd = hex.EncodeToString(sha.Sum(nil))
	return u.Password == pwd
}
