package protocol

type RegisterRequest struct {
	UserName string `json:"username"`
	PassWord string `json:"password"`
	NickName string `json:"nickname"`
}

type RegisterResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type LoginRequest struct {
	UserName string `json:"username"`
	PassWord string `json:"password"`
}

type LoginResponse struct {
	Status  int    `json:"status"`
	UserId  int64  `json:"userId"`
	Message string `json:"message"`
	Token   string `json:"token"`
}

type LogoutRequest struct {
	Token  string `json:"token"`
	UserId int64  `json:"userId"`
}

type LogoutResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}

type GetRequest struct {
	UserId int64  `json:"userId"`
	Token  string `json:"token"`
}

type GetResponse struct {
	Status   int    `json:"status"`
	UserName string `json:"username"`
	NickName string `json:"nickname"`
	PicName  string `json:"pic-name"`
}

type UpdateRequest struct {
	UserId   int64  `json:"userId"`
	Token    string `json:"token"`
	NickName string `json:"nickname"`
	PicName  string `json:"pic-name"`
}

type UpdateResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
}
