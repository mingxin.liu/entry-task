package config

import "time"

const (
	MysqlDB string = "root:Shopee0331@(127.0.0.1:3306)/test_db?charset=utf8"
	// ConnMaxLifetime 数据库一个连接的最大生命周期.
	ConnMaxLifetime time.Duration = 14400 * time.Second
	// MaxIdleConns 连接池中最大空闲连接数.
	MaxIdleConns int = 500
	// MaxOpenConns 同时连接数据库中最多连接数.
	MaxOpenConns int = 500

	RedisAddr     = "127.0.0.1:6379"
	RedisPwd      = ""
	RedisPoolSize = 30
	//// TCPServerLogPath TCP服务日志.
	//TCPServerLogPath string = "./log/tcp_server.log"
	//// TCPServerAddr tcp server ip:port.
	//TCPServerAddr string = ":3194"
	//// TCPClientPoolSize 客户端tcp连接池大小.
	//TCPClientPoolSize int = 2000
	//
	//// HTTPServerLogPath HTTP服务日志.
	//HTTPServerLogPath string = "./log/http_server.log"
	//// HTTPServerAddr HTTP服务地址.
	//HTTPServerAddr string = ":8080"
)
