# 用户管理系统

## 功能需求

实现一个用户管理系统，用户可以登录、拉取和编辑他们的profiles。 

用户可以通过在Web页面输入username和password登录，backend系统负责校验用户身份。

成功登录后，页面需要展示用户的相关信息；否则页面展示相关错误。

成功登录后，用户可以编辑以下内容：

1. 上传profile picture

2. 修改nickname（需要支持Unicode字符集，utf-8编码）

用户信息包括：

1. userid （主键，用户无感知）

2. username（唯一）

3. nickname

4. profile picture

需要提前将初始用户数据插入数据库用于测试。确保测试数据库中包含10,000,000条用户账号信息。

## 设计要求

* 分别实现HTTP server和TCP server，主要的功能逻辑放在TCP server实现
* Backend鉴权逻辑需要在TCP server实现
* 用户账号信息必须存储在MySQL数据库。通过MySQL Go client连接数据库
* 使用基于Auth/Session Token的鉴权机制，Token存储在redis，避免使用JWT等加密的形式。
* TCP server需要提供RPC API，RPC机制希望自己设计实现
* Web server不允许直连MySQL、Redis。所有HTTP请求只处理API和用户输入，具体的功能逻辑和数据库操作，需要通过RPC请求TCP server完成
* 尽可能使用Go标准库
* 安全性
* 鲁棒性
* 性能

## 开发环境

* 操作系统：macOS Catalina 10.15.6
* Go:1.15.6
* Mysql: 8.0.22
* Redis: 6.0.10


## 具体实现
### mysql数据库 —— tbl_user
<br>

```sql
CREATE TABLE `tbl_user`(
    `id` bigInt(20) NOT NULL AUTO_INCREMENT,
    `user_name` varchar(255) NOT NULL DEFAULT ''
    `password` varchar(255) NOT NULL DEFAULT '',
    `nick_name` varchar(255) NOT NULL DEFAULT '',
    `pic_name` varchar(255) DEFAULT '',
    PRIMARY KEY (`id`),
    UNIQUE KEY (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```
### redis设计

redis 分为两部分，分别以"user_" + userId 和 "token_" + userId为主键，以存储用户信息和token。

在对用户信息进行更新之后，别忘了删除redis中旧的数据。

| key              | value                                           |
| -----------------| ------------------------------------------------|
| "user_" + userId | {[user_name, “”],[nick_name, “”], [pic_name,“”]} 
| "token_" + userId| {[token,“”]}                                    |

### 代码结构

```bash
usermana
├── README.md               //程序文档
├── benchmark               //压力测试文件
├── userapi                 //接口服务器
        ├──api                      //http 接口
        ├──image                    //用户头像
        ├──rpcclient                //rpc client
        ├──static                   //前端静态文件
├── userserver              //rpc 服务器
        ├── config                  //配置文件
        ├── dao                     //数据操作
            ├── userdb                      //操作mysql
            ├── userredis                   //操作redis
        ├── handler                 //rpc业务逻辑
        ├── log                     //日志相关文件
        ├── model                   //user 对象结构	
        ├── protocol                //主要定义一些通讯的数据结构
```

### API

|function | URL | method |
| ------ | ------ | ------ |
| 用户注册 | http://localhost:8080/register | post |
| 用户登录 | http://localhost:8080/login | post |
| 获取用户信息 | http://localhost:8080/user/:username | get |
| 更新用户信息 | http://localhost:8080/user/:username | put |

### 流程图

#### Login流程图
![diagram1](./userapi/image/resource/loginProcess.png)

#### GetProfile流程图
![diagram1](./userapi/image/resource/getProcess.png)

#### UpdateProfile流程图
![diagram1](./userapi/image/resource/updateProcess.png)

### 压测结果

#### 登录接口  Login:

##### 固定用户 200 并发：
>Total Requests(20000) - Concurrency(200) - Cost(3.637445267s) - QPS(5499/sec)
##### 固定用户 2000 并发:	
>Total Requests(20000) - Concurrency(2000) - Cost(3.911255983s) - QPS(5114/sec)
##### 随机用户 200 并发：
>Total Requests(20000) - Concurrency(200) - Cost(3.791910702s) - QPS(5275/sec)
##### 随机用户 2000 并发：
>Total Requests(20000) - Concurrency(2000) - Cost(4.292503311s) - QPS(4660/sec)

#### 获取用户信息接口 :
##### 固定用户 200 并发：
>Total Requests(20000) - Concurrency(200) - Cost(3.25586616s) - QPS(6143/sec)
##### 固定用户 2000 并发:
>Total Requests(20000) - Concurrency(2000) - Cost(3.277153482s) - QPS(6103/sec)
##### 随机用户 200 并发：
>Total Requests(20000) - Concurrency(200) - Cost(3.414126753s) - QPS(5859/sec)
##### 随机用户 2000 并发：
>Total Requests(20000) - Concurrency(2000) - Cost(3.596144314s) - QPS(5562/sec)

#### 修改信息接口 Put：
##### 固定用户 200 并发：
>Total Requests(20000) - Concurrency(200) - Cost(3.731844512s) - QPS(5360/sec)
##### 固定用户 2000 并发:
>Total Requests(20000) - Concurrency(2000) - Cost(4.799399727s) - QPS(4168/sec)
##### 随机用户 200 并发：
>Total Requests(20000) - Concurrency(2000) - Cost(5.19393699s) - QPS(3851/sec)
##### 随机用户 2000 并发:
>Total Requests(20000) - Concurrency(200) - Cost(5.408707103s) - QPS(3698/sec)


	



