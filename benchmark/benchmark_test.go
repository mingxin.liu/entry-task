package tset

import (
	"bytes"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func BenchmarkLogin(b *testing.B) {
	client := &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout: 30 * time.Second,
				//KeepAlive: -1 * time.Second, //压测的时候保持长连接有助于提升性能
				KeepAlive: 30 * time.Second, //压测的时候保持长连接有助于提升性能
			}).DialContext,
			MaxConnsPerHost:     200, //最大200条连接
			MaxIdleConns:        200, //最大200条闲置连接
			MaxIdleConnsPerHost: 200,
			IdleConnTimeout:     time.Duration(10) * time.Second,
		},
	}

	b.ResetTimer()                       //重置统计时间
	b.SetParallelism(200 / 8)            //这里是个坑，goroutine_num=parallelism * gomaxprocs，mac默认的gomaxprocs是8，因此开启200个协程就需要设置parallelism为200/8=25
	b.RunParallel(func(pb *testing.PB) { //多协程并行执行b.N次，b.N的值不是确定的，会通过多次执行benchmark函数来试探执行时间内大概能执行多少次，默认会执行1s的时间
		for pb.Next() {
			username := "bot" + strconv.Itoa(rand.Intn(10000000))
			d := []byte("123")
			m := md5.New()
			m.Write(d)
			password := hex.EncodeToString(m.Sum(nil))
			requestBody := fmt.Sprintf(`{"username":"%s","password": "%s"}`, username, password)
			var jsonStr = []byte(requestBody)
			req, _ := http.NewRequest("POST", "http://localhost:8080/login", bytes.NewBuffer(jsonStr))
			req.Header.Add("Content-type", "application/json")
			rsp, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			rsp.Body.Close()
		}
	})
}

func BenchmarkGet(b *testing.B) {
	client := &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout: 30 * time.Second,
				//KeepAlive: -1 * time.Second, //压测的时候保持长连接有助于提升性能
				KeepAlive: 30 * time.Second, //压测的时候保持长连接有助于提升性能
			}).DialContext,
			MaxConnsPerHost:     200, //最大200条连接
			MaxIdleConns:        200, //最大200条闲置连接
			MaxIdleConnsPerHost: 200,
			IdleConnTimeout:     time.Duration(10) * time.Second,
		},
	}

	b.ResetTimer()                       //重置统计时间
	b.SetParallelism(200 / 8)            //这里是个坑，goroutine_num=parallelism * gomaxprocs，mac默认的gomaxprocs是8，因此开启200个协程就需要设置parallelism为200/8=25
	b.RunParallel(func(pb *testing.PB) { //多协程并行执行b.N次，b.N的值不是确定的，会通过多次执行benchmark函数来试探执行时间内大概能执行多少次，默认会执行1s的时间
		for pb.Next() {
			userId := strconv.Itoa(rand.Intn(10000000))
			req, _ := http.NewRequest("Get", "http://localhost:8080/user/"+userId, nil)
			req.Header.Add("Content-type", "application/json")
			req.AddCookie(&http.Cookie{Name: "token", Value: "test", Expires: time.Now().Add(120 * time.Second)})
			rsp, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			rsp.Body.Close()
		}
	})
}

func BenchmarkPut(b *testing.B) {
	client := &http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout: 30 * time.Second,
				//KeepAlive: -1 * time.Second, //压测的时候保持长连接有助于提升性能
				KeepAlive: 30 * time.Second, //压测的时候保持长连接有助于提升性能
			}).DialContext,
			MaxConnsPerHost:     200, //最大200条连接
			MaxIdleConns:        200, //最大200条闲置连接
			MaxIdleConnsPerHost: 200,
			IdleConnTimeout:     time.Duration(10) * time.Second,
		},
	}

	b.ResetTimer()                       //重置统计时间
	b.SetParallelism(200 / 8)            //这里是个坑，goroutine_num=parallelism * gomaxprocs，mac默认的gomaxprocs是8，因此开启200个协程就需要设置parallelism为200/8=25
	b.RunParallel(func(pb *testing.PB) { //多协程并行执行b.N次，b.N的值不是确定的，会通过多次执行benchmark函数来试探执行时间内大概能执行多少次，默认会执行1s的时间
		for pb.Next() {
			userId := strconv.Itoa(rand.Intn(10000000))
			requestBody := fmt.Sprintf(`{"nickname":"new"}`)
			var jsonStr = []byte(requestBody)
			req, _ := http.NewRequest("PUT", "http://localhost:8080/user/"+userId, bytes.NewBuffer(jsonStr))
			req.AddCookie(&http.Cookie{Name: "token", Value: "test", Expires: time.Now().Add(120 * time.Second)})
			req.Header.Add("Content-type", "application/json")
			rsp, err := client.Do(req)
			if err != nil {
				fmt.Println(err)
				panic(err)
			}
			rsp.Body.Close()
		}
	})
}
