package rpcclient

import (
	"entryTask/userserver/protocol"
	"fmt"
	"net/rpc"
)

var client = new(rpc.Client)

func init() {
	fmt.Println("rpc client: register start")
	var err error
	client, err = rpc.Dial("tcp", "localhost:1231")
	if err != nil {
		fmt.Println("rpc client: rpc dial failed")
		return
	}
	fmt.Println("rpc client: rpc dial success")
}

func Register(request protocol.RegisterRequest) (response protocol.RegisterResponse) {
	// 调用 rpc server
	client.Call("RPCServer.Register", request, &response)
	fmt.Println("rpc client: register done")
	return response
}

func Login(request protocol.LoginRequest) (response protocol.LoginResponse) {
	// 调用 rpc server
	client.Call("RPCServer.Login", request, &response)
	fmt.Println("rpc client: login done")
	return response
}

func Logout(request protocol.LogoutRequest) (response protocol.LogoutResponse) {
	// 调用 rpc server
	client.Call("RPCServer.Logout", request, &response)
	fmt.Println("rpc client: logout done")
	return response
}

func GetProfile(request protocol.GetRequest) (response protocol.GetResponse) {
	// 调用 rpc server
	client.Call("RPCServer.GetProfile", request, &response)
	fmt.Println("rpc client: getProfile done")
	return response

}

func Update(request protocol.UpdateRequest) protocol.UpdateResponse {
	var response protocol.UpdateResponse
	client.Call("RPCServer.Update", request, &response)
	return response
}
