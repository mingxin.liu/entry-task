package main

import (
	"crypto/md5"
	"encoding/hex"
	"entryTask/userapi/rpcclient"
	"entryTask/userserver/protocol"
	"fmt"
	"github.com/gin-gonic/gin"
	"io"
	"net/http"
	"os"
	"strconv"
)

const (
	success int = 0
	failed  int = 1
	wrong   int = 2
)

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("../static/*.html")
	r.StaticFS("/static", http.Dir("../static"))
	r.StaticFS("/image", http.Dir("../image"))

	r.GET("/", func(c *gin.Context) {
		token, err1 := c.Cookie("token")
		userId, err2 := c.Cookie("userId")
		fmt.Println(token)
		fmt.Println(userId)
		if err1 != nil || err2 != nil || token == "" || userId == "" {
			c.HTML(http.StatusOK, "login.html", nil)
			return
		} else {
			c.Redirect(http.StatusTemporaryRedirect, "http://localhost:8080/static/?userId="+userId)
		}

	})

	// 注册接口
	r.POST("/register", func(c *gin.Context) {
		request := protocol.RegisterRequest{}
		if c.ShouldBind(&request) != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "注册失败",
			})
			return
		}
		fmt.Println(request.UserName)
		fmt.Println(request.NickName)
		fmt.Println(request.PassWord)

		d := []byte(request.PassWord)
		m := md5.New()
		m.Write(d)
		request.PassWord = hex.EncodeToString(m.Sum(nil))
		fmt.Println(request.PassWord)

		//调用rpc client
		response := rpcclient.Register(request)
		if response.Status == success {
			c.JSON(http.StatusOK, gin.H{
				"Code": success,
				"Msg":  "注册成功",
			})
			return
		} else if response.Status == failed {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "注册失败",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"Code": wrong,
				"Msg":  "出现异常",
			})
			return
		}
	})

	// 登陆接口
	r.POST("/login", func(c *gin.Context) {
		request := protocol.LoginRequest{}
		if c.ShouldBind(&request) != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "登陆失败",
			})
			return
		}

		//调用rpc client
		response := rpcclient.Login(request)
		if response.Status == success {
			c.SetCookie("userId", strconv.FormatInt(response.UserId, 10), 360000, "/", "localhost", false, true)
			c.SetCookie("token", response.Token, 360000, "/", "localhost", false, true)
			c.JSON(http.StatusOK, gin.H{
				"Code":   success,
				"Msg":    "登陆成功",
				"UserId": response.UserId,
			})
			return
		} else if response.Status == failed {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "登陆失败",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"Code": wrong,
				"Msg":  response.Message,
			})
			return
		}

	})

	// 登出接口
	r.POST("/logout", func(c *gin.Context) {
		data := protocol.LogoutRequest{}
		if c.ShouldBind(&data) != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "登出失败",
			})
			return
		}
		token, err := c.Cookie("token")
		if token == "" || err != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "登出失败",
			})
			return
		}
		request := protocol.LogoutRequest{Token: token, UserId: data.UserId}
		//调用rpc client
		response := rpcclient.Logout(request)
		if response.Status == success {
			c.SetCookie("userId", "", -1, "/", "localhost", false, true)
			c.SetCookie("token", "", -1, "/", "localhost", false, true)
			c.JSON(http.StatusOK, gin.H{
				"Code": success,
				"Msg":  "登出成功",
			})
			return
		} else if response.Status == failed {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "登出失败",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"Code": wrong,
				"Msg":  response.Message,
			})
			return
		}

	})

	// 查询用户信息接口
	r.GET("/user/:userId", func(c *gin.Context) {
		//获取 userId, token
		id := c.Param("userId")
		userId, err1 := strconv.ParseInt(id, 10, 64)
		token, err2 := c.Cookie("token")
		if id == "" || err1 != nil || err2 != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "请重新登陆",
			})
			return
		}

		//调用 rpc client
		response := rpcclient.GetProfile(protocol.GetRequest{UserId: userId, Token: token})
		if response.Status == success {
			c.JSON(http.StatusOK, gin.H{
				"Code":     success,
				"UserName": response.UserName,
				"NickName": response.NickName,
				"ImageUrl": response.PicName,
			})
			return
		} else if response.Status == failed {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "查询失败",
			})
			return
		} else {
			c.JSON(http.StatusOK, gin.H{
				"Code": wrong,
				"Msg":  "出现异常",
			})
			return
		}

	})

	// 更新用户信息 Nickname 或 Picture
	r.PUT("/user/:userId", func(c *gin.Context) {
		// 绑定用户信息
		id := c.Param("userId")
		userId, err1 := strconv.ParseInt(id, 10, 64)
		token, err2 := c.Cookie("token")
		if id == "" || err1 != nil || err2 != nil {
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "请重新登陆",
			})
			return
		}
		request := protocol.UpdateRequest{UserId: userId, Token: token}
		if c.ShouldBind(&request) != nil {
			fmt.Println(c.ShouldBind(&request))
			c.JSON(http.StatusOK, gin.H{
				"Code": failed,
				"Msg":  "更新失败",
			})
			return
		}
		fmt.Println(request)

		// update Nickname
		if request.NickName != "" {
			response := rpcclient.Update(request)
			if response.Status == success {
				c.JSON(http.StatusOK, gin.H{
					"Code": success,
					"Msg":  "Nickname更新成功",
				})
				return
			} else if response.Status == failed {
				c.JSON(http.StatusOK, gin.H{
					"Code": failed,
					"Msg":  "Nickname更新失败",
				})
				return
			} else {
				c.JSON(http.StatusOK, gin.H{
					"Code": wrong,
					"Msg":  response.Message,
				})
				return
			}
		} else {
			file, header, err := c.Request.FormFile("file")
			defer file.Close()
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"Code": failed,
					"Msg":  "图片上传失败",
				})
				return
			}
			request.PicName = "../image/" + id + header.Filename
			fmt.Println("picture:  " + request.PicName)
			out, err := os.Create(request.PicName)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"Code": failed,
					"Msg":  "图片上传失败",
				})
				return
			}
			defer out.Close()

			_, err = io.Copy(out, file)
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"Code": failed,
					"Msg":  "图片上传失败",
				})
				return
			}

			// 调用 rpc client
			response := rpcclient.Update(request)
			if response.Status == success {
				c.JSON(http.StatusOK, gin.H{
					"Code":     success,
					"ImageUrl": request.PicName,
				})
				return
			} else if response.Status == failed {
				c.JSON(http.StatusOK, gin.H{
					"Code": failed,
					"Msg":  "Picture更新失败",
				})
				return
			} else {
				c.JSON(http.StatusOK, gin.H{
					"Code": wrong,
					"Msg":  response.Message,
				})
				return
			}
		}
	})

	err := r.Run("localhost:8080")
	if err != nil {
		return
	} //运行run命令并且指定地址和端口,

}
